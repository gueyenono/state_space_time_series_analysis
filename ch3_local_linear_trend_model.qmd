# The Local Linear Trend Model

The local linear trend model is obtained by adding a *slope component* to to the local level model. The univariate formulation of the model is:

$$
\begin{align}
  y_t &= \mu_t + \varepsilon_{y,t}, & \mathcal{N}(0, \sigma_{y,t}^{2}) \\
  \mu_t &= \mu_{t-1} + \beta_{t-1} + \varepsilon_{\mu,t}, & \mathcal{N}(0, \sigma_{\mu, t}^{2}) \\
  \beta_{t} &= \beta_{t-1} + \varepsilon_{\beta,t}, & \mathcal{N}(0, \sigma_{\beta,t}^{2})
\end{align}
$$

-   $\mu_t$ is the dynamic level of $y_t$
-   $\varepsilon_{y,t}$ is the white noise observation disturbance.
-   $\sigma_{y,t}^{2}$ is the observation variance
-   $\varepsilon_{\mu, t}$ is the level disturbance
-   $\sigma_{\mu,t}^{2}$ is the level variance
-   $\beta_t$ is the slope component of the state
-   $\varepsilon_{\beta,t}$ is the slope disturbance parameter
-   $\sigma_{\beta,t}^{2}$ is the slope disturbance variance

The slope component is $\beta_t$ and it is added to the level. The local linear trend model contains two state equations: one for modeling the level and another one for modeling the trend/slope.

The matrix formulation of the model is:

$$
\begin{align}
y_t &= F_t \theta_t + v_t, & \mathcal{N}_m(0, V_t) \\
\theta_t &= G_t \theta_{t-1} + w_t, & \mathcal{N}_p(0, W_t)
\end{align}
$$

-   $m = 2$ is the number of components in the observation. Here we observe only one series
-   $p = 2$ is the number of components of the state: the level and the slope
-   $F_t = \begin{bmatrix} 1 & 0 \end{bmatrix}$ is the *observation matrix*
-   $\theta_t = \begin{bmatrix} \mu_t \\ \beta_t \end{bmatrix}$ is the state vector
-   $v_t$ is the observation disturbance term and $V_t = \begin{bmatrix} \sigma_{y,t}^{2} \end{bmatrix}$ is the observation variance
-   $G_t = \begin{bmatrix} 1 & 1 \\ 0 & 1 \end{bmatrix}$ is the *transition matrix*
-   $w_t$ is the state disturbance term and $W_t = \begin{bmatrix} \sigma_{\mu_,t}^{2} & 0 \\ 0 & \sigma_{\beta_,t}^{2} \end{bmatrix}$ is the state variance

In its most explicit form, the matrix formulation of the model is:

$$
\begin{align}
y_t &= \begin{bmatrix} 1 & 0 \end{bmatrix} \begin{bmatrix} \mu_t \\ \beta_t \end{bmatrix} + v_t, & \mathcal{N}_m(0, \sigma_{y,t}^2) \\
\begin{bmatrix} \mu_t \\ \beta_t \end{bmatrix} &= G_t \begin{bmatrix} \mu_{t-1} \\ \beta_{t-1} \end{bmatrix} + w_t, & \mathcal{N}_p(0, \begin{bmatrix} \sigma_{\mu_,t}^{2} & 0 \\ 0 & \sigma_{\beta_,t}^{2} \end{bmatrix})
\end{align}
$$

## Deterministic level and slope

### The model

If the level and slope are deterministic, then their disturbance parameters are fixed to 0 (i.e. $\varepsilon_{\mu,t} = \varepsilon_{\beta,t} = 0$). In order to understand the demonstration below, it may be easier to work backwards. That is, at each time period $t$, determine the expression for the slope $\beta_t$, then the expression of the level $\mu_t$ and finally the expression of the observation $y_t$.

$$
\begin{align}
\text{for t = 2}: && y_2 &= \mu_2 + \varepsilon_{y,2}, \\
                  && \mu_2 &= \mu_1 + \beta_1 + \varepsilon_{\mu,2} \\
                  &&       &= \mu_1 + \beta_1 \\
                  && \beta_2 &= \beta_1 + \varepsilon_{\beta,2} \\
                  &&         &= \beta_1 \\
\text{for t = 3}: && y_3 &= \mu_3 + \varepsilon_{y,3}, \\
                  && \mu_3 &= \mu_2 + \beta_{2} + \varepsilon_{\mu,2} \\
                  &&       &= \mu_1 + \beta_1 + \beta_2 \\
                  &&       &= \mu_1 + \beta_1 + \beta_1 \\
                  &&       &= \mu_1 + 2\beta_1 \\
                  && \beta_3 &= \beta_2 + \varepsilon_{\beta,t} \\
                  && \beta_3 &= \beta_1 \\
\text{for t = 4}: && y_4 &= \mu_4 + \varepsilon_{y,4}, \\
                  && \mu_4 &= \mu_3 + \beta_4 + \varepsilon_{\mu,4} \\
                  &&       &= \mu_1 + 2\beta_1 + \beta_4 \\
                  &&       &= \mu_1 + 2\beta_1 + \beta_1 \\
                  &&       &= \mu_1 + 3\beta_1 \\
                  && \beta_4 &= \beta_3 + \varepsilon_{\beta,t} \\
                  &&         &= \beta_1 \\
\dots                  
\end{align}
$$

So the local linear trend model can be summarized as:

$$
y_t = \mu_1 + \beta_1(t-1) + \varepsilon_{y,t}
$$

This equation is essentially a linear regression equation. $\mu_1$ is a constant term, $\beta_1$ is the slope coefficient, $(t-1)$ is the predictor variable and $\varepsilon_{y,t}$ is the error term.

### Estimation

```{r}
library(magrittr)

uk_ksi_raw <- readr::read_lines(file = here::here("data/UKdriversKSI.txt"), skip = 1)
uk_ksi <- tibble::tibble(
  date = seq(from = as.Date("1969-01-01"), by = "month", length.out = length(uk_ksi_raw)),
  value = as.numeric(uk_ksi_raw),
  log_value = log(value)
)
```

Here is a plot of the data:

```{r}
library(ggplot2)

ggplot(data = uk_ksi, mapping = aes(x = date, y = value)) +
  geom_line() +
  labs(x = NULL, y = NULL, title = "UK Killed or Serious Injured (KSI)") +
  theme_bw()
```

Determine the initial value of the hyperparameter (i.e. the observation variance)

```{r}
lltm1_initial_value <- var(uk_ksi$log_value)
```

Function for building the local linear trend model

```{r}
func_lltm1_mod <- function(parm){
  dlm::dlmModPoly(
    order = 2, # local linear trend
    dV = exp(parm), # Observation variance
    dW = c(0, 0) # Deterministic level and slope
  )
}
```

Estimate the model's hyperparameter with MLE

```{r}
lltm1_mle_est <- dlm::dlmMLE(
  y = uk_ksi$log_value,
  parm = log(lltm1_initial_value),
  build = func_lltm1_mod
)
```

Convergence

```{r}
lltm1_mle_est$convergence
```

Build the model with our estimates

```{r}
lltm1_mod <- func_lltm1_mod(parm = lltm1_mle_est$par)
```

Estimate the model's state with the Kalman filter

```{r}
lltm1_filtered <- dlm::dlmFilter(y = uk_ksi$log_value, mod = lltm1_mod)
```

Estimate the model's state with the Kalman smoother

```{r}
lltm1_smoothed <- dlm::dlmSmooth(y = uk_ksi$log_value, mod = lltm1_mod)
```

Value of the log-likelihood at convergence

```{r}
source(here::here("R/log-lik_aic.R"))
lltm1_Ld <- get_log_likelihood(dlm_filtered = lltm1_filtered, n_state_vars = 2)
lltm1_Ld
```

Value of the log-likelihood to be compared with the book's

```{r}
lltm1_Ld / nrow(uk_ksi) # The book's value is 0.4140728
```

The maximum likelihood estimate of the observation variance

```{r}
as.numeric(lltm1_mod$V)
```

The maximum likelihood estimate of the initial value of the level

```{r}
lltm1_smoothed$s[2, 1]
```

The maximum likelihood estimate of the initial value of the trend

```{r}
lltm1_smoothed$s[2, 2]
```

Since the level and slopes are deterministic, the components of the state variance are 0 ($\sigma_{\mu,t}^{2} = \sigma_{\beta,t}^{2} = 0$).

### Visualization of results

Visualize level

```{r}
source(here::here("R/visualizations.R"))

visualize_level(dlm_filtered = lltm1_filtered, dlm_smoothed = lltm1_smoothed, series_name = "UK KSI")
```

Visualize slope

```{r}
visualize_other_state_components(dlm_filtered = lltm1_filtered, dlm_smoothed = lltm1_smoothed, component_names = "Slope")
```

### Diagnostic tests

Let's visualize the residuals:

```{r}
visualize_residuals(dlm_filtered = lltm1_filtered, dlm_smoothed = lltm1_smoothed)
```

Let's run the diagnostic tests

```{r}
source(here::here("R/diagnostic_tests.R"))
diagnostic_tests(dlm_filtered = lltm1_filtered,
                n_state_vars = 2,
                n_hyper_params = 1,
                n_lags = 15) %>%
  flextable::flextable()
```

The model's AIC is:

```{r}
source(here::here("R/log-lik_aic.R"))
lltm1_aic <- get_AIC(log_likelihood = lltm1_Ld, n_state_vars = 2, n_hyper_params = 1)
lltm1_aic
```

The AIC to be compared to the book's AIC is:

```{r}
lltm1_aic/nrow(uk_ksi) # The book's value: -0.796896
```

This is a summary of the AIC values obtained from the various models applied to the UK KSI data for far:

```{r}
tibble::tibble(
  Model = c("Local level model (deterministic level)",
            "Local level model (stochastic level)",
            "Local linear trend model (deterministic level and slope)"),
  AIC = c(-124.769, -239.9174, lltm1_aic)
) %>%
  flextable::flextable()
```

So far, the best model for the log of UK KSI is the local level model with a stochastic level.

## Stochastic level and slope

### The model

$$
\begin{align}
  y_t &= \mu_t + \varepsilon_{y,t}, & \mathcal{N}(0, \sigma_{y,t}^{2}) \\
  \mu_t &= \mu_{t-1} + \beta_{t-1} + \varepsilon_{\mu,t}, & \mathcal{N}(0, \sigma_{\mu, t}^{2}) \\
  \beta_{t} &= \beta_{t-1} + \varepsilon_{\beta,t}, & \mathcal{N}(0, \sigma_{\beta,t}^{2})
\end{align}
$$

### Estimation

Determine the initial values of the 3 hyperparameters: the observation, level and slope variances

```{r}
lltm2_initial_values <- c(var(uk_ksi$log_value), 0.001, 0.001)
lltm2_initial_values
```

Function for building the local linear trend model with a stochastic level and slope

```{r}
func_lltm2_mod <- function(parm){
  dlm::dlmModPoly(
    order = 2, # local linear trend
    dV = exp(parm[1]), # Observation variance
    dW = exp(parm[2:3]) # Level and slope variances
  )
}
```

Estimate the model's hyperparameters with MLE

```{r}
lltm2_mle_est <- dlm::dlmMLE(
  y = uk_ksi$log_value,
  parm = log(lltm2_initial_values),
  build = func_lltm2_mod
)
```

Convergence

```{r}
lltm2_mle_est$convergence
```

Build the model with our estimates

```{r}
lltm2_mod <- func_lltm2_mod(parm = lltm2_mle_est$par)
```

Estimate the model's state with the Kalman filter

```{r}
lltm2_filtered <- dlm::dlmFilter(y = uk_ksi$log_value, mod = lltm2_mod)
```

Estimate the model's state with the Kalman smoother

```{r}
lltm2_smoothed <- dlm::dlmSmooth(y = uk_ksi$log_value, mod = lltm2_mod)
```

Value of the log-likelihood at convergence

```{r}
lltm2_Ld <- get_log_likelihood(dlm_filtered = lltm2_filtered, n_state_vars = 2)
lltm2_Ld
```

Value of the log-likelihood to be compared with the book's

```{r}
lltm2_Ld / nrow(uk_ksi) # The book's value is 0.6247935
```

The maximum likelihood estimate of the observation variance

```{r}
as.numeric(lltm2_mod$V)
```

The maximum likelihood estimates of the state disturbance variances

```{r}
diag(lltm2_mod$W)
```

The maximum likelihood estimate of the initial value of the level

```{r}
lltm2_smoothed$s[2, 1]
```

The maximum likelihood estimate of the initial value of the slope

```{r}
lltm2_smoothed$s[2, 2]
```

### Visualization of results

Visualize level

```{r}
visualize_level(dlm_filtered = lltm2_filtered, dlm_smoothed = lltm2_smoothed, series_name = "UK KSI")
```

Visualize slope

```{r}
lltm2_state_compoments <- visualize_other_state_components(dlm_filtered = lltm2_filtered, dlm_smoothed = lltm2_smoothed, component_names = "Slope")
lltm2_state_compoments[[1]]
```

#### Diagnostic tests

We begin by visualizing the residuals

```{r}
visualize_residuals(dlm_filtered = lltm2_filtered, dlm_smoothed = lltm2_smoothed)
```

Here are the actual diagnostic test results

```{r}
diagnostic_tests(dlm_filtered = lltm2_filtered, n_state_vars = 2, n_hyper_params = 3, n_lags = 15) %>%
  flextable::flextable()
```

The model fails on the criteria of independence and normality residuals. Here is the model's AIC

```{r}
lltm2_aic <- get_AIC(log_likelihood = lltm2_Ld, n_state_vars = 2, n_hyper_params = 3)
lltm2_aic
```

Here is the AIC to be compared to the book's AIC ($-1.1975$)

```{r}
lltm2_aic / nrow(uk_ksi)
```

Let's see how this model measures up to the other models previously used on the UK KSI data

```{r}
tibble::tibble(
  Model = c("Local level model (deterministic level)",
            "Local level model (stochastic level)",
            "Local linear trend model (deterministic level and slope)",
            "Local linear trend model (stochastic level and slope"),
  AIC = c(-124.769, -239.9174, lltm1_aic, lltm2_aic)
) %>%
  flextable::flextable()
```

So far, the best model still remains the local level model with a stochastic level.

### Stochastic level and deterministic slope

The MLE estimate of the slope's disturbance variance in the previous section is essentially 0

```{r}
diag(lltm2_mod$W)[2]
```

So it may be a good idea to model the slope as a deterministic component. Let's do it!

Initial values of the 2 hyperparameters: the observation variance ($\sigma_{y,t}^{2}$) and the level variance ($\sigma_{\mu,t}^{2}$):

```{r}
lltm3_initial_values <- c(var(uk_ksi$log_value), 0.001)
lltm3_initial_values
```

Function for building the model

```{r}
func_lltm3_mod <- function(parm){
  dlm::dlmModPoly(
    order = 2,
    dV = exp(parm[1]), # observation variance
    dW = c(exp(parm[2]), 0) # level variance (exp(parm[2])) and deterministic slope (0)
  )
}
```

Estimate the model parameters with MLE

```{r}
lltm3_mle_est <- dlm::dlmMLE(
  y = uk_ksi$log_value,
  parm = log(lltm3_initial_values),
  build = func_lltm3_mod
)
```

Do we have convergence?

```{r}
lltm3_mle_est$convergence
```

Build the model with our MLE estimates

```{r}
lltm3_mod <- func_lltm3_mod(parm = lltm3_mle_est$par)
```

State estimation with Kalman filter and smoother

```{r}
lltm3_filtered <- dlm::dlmFilter(y = uk_ksi$log_value, mod = lltm3_mod)
lltm3_smoothed <- dlm::dlmSmooth(y = lltm3_filtered, mod = lltm3_mod) # I'm using the filtered object insteaad of the original series (it's the same!)
```

Value of the log-likelihood at convergence

```{r}
lltm3_Ld <- get_log_likelihood(dlm_filtered = lltm3_filtered, n_state_vars = 2)
lltm3_Ld
```

Value of the log-likelihood to be compared with the book

```{r}
lltm3_Ld / nrow(uk_ksi)
```

The maximum likelihood estimate of the observation variance:

```{r}
as.numeric(lltm3_mod$V)
```

The maximum likelihood estimate of the level variance (the slope's variance is 0 because it is deterministic)

```{r}
diag(lltm3_mod$W)
```

The maximum likelihood estimate of the initial value of the level is:

```{r}
lltm3_smoothed$s[2, 1]
```

The maximum likelihood estimate of the intial value of the slope (the slope is constant/deterministic in this model anyway)

```{r}
lltm3_smoothed$s[2, 2]
```

Visualize the series' level

```{r}
visualize_level(dlm_filtered = lltm3_filtered, dlm_smoothed = lltm3_smoothed, series_name = "UK KSI")
```

Visualize the series' slope

```{r}
visualize_other_state_components(dlm_filtered = lltm3_filtered, dlm_smoothed = lltm3_smoothed, component_names = "slope")[[1]]
```

Diagnostic tests now...

Visualize the residuals

```{r}
visualize_residuals(dlm_filtered = lltm3_filtered, dlm_smoothed = lltm3_smoothed)
```

Diagnostic test results

```{r}
diagnostic_tests(dlm_filtered = lltm3_filtered, n_state_vars = 2, n_hyper_params = 2, n_lags = 15) %>%
  flextable::flextable()
```

The model's AIC is

```{r}
lltm3_aic <- get_AIC(log_likelihood = lltm3_Ld, n_state_vars = 2, n_hyper_params = 2)
lltm3_aic
```

The model's AIC to be compared to the book's

```{r}
lltm3_aic / nrow(uk_ksi)
```

**Despite the inclusion of a time trend, the local linear trend model fails to perform better than the local level model (with a stochastic level) whose AIC was -239.92**. So it may be a better idea to remove it from the analysis of UK KSI.

## The local linear trend model for Finnish fatalities

In this section we model the log of annual road traffic fatalities in Finland with a local linear trend model (1970 - 2003). Let's import the data and load the functions that automate the modeling process.

```{r}
library(magrittr)

norfin_raw <- readr::read_table(file = here::here("data/NorwayFinland.txt"), skip = 1, col_names = c("year", "norway", "finland"))

finnish_fatal <- norfin_raw %>%
  dplyr::select(year, value = finland) %>%
  dplyr::mutate(log_value = log(value))

source(file = here::here("R/full_dlm_spec.R"))
source(file = here::here("R/diagnostic_tests.R"))
source(file = here::here("R/full_visualizations.R"))
```

Estimate the model

```{r}
lltm4_full_mod <- full_dlm_modeling(
  series = finnish_fatal$log_value,
  state_components = c("level", "slope"),
  deterministic_components = NULL
)
```

-   The observation variance is:

```{r}
lltm4_full_mod$obs_variance
```

-   The level and slope variances are:

```{r}
lltm4_full_mod$state_variance
```

-   The maximum likelihood estimate of the initial value of the level

```{r}
lltm4_full_mod$smoothed$s[2, 1]
```

-   The maximum likelihood estimate of the initial value of the slope

```{r}
lltm4_full_mod$smoothed$s[2, 2]
```

Now let's visualize the results

```{r}
lltm4_full_viz <- full_dlm_viz(dlm_model = lltm4_full_mod)
```

-   The series + the smooth level

```{r}
lltm4_full_viz$level_with_conf_band
```

-   The stochastic slope

```{r}
lltm4_full_viz$slope +
  ggplot2::geom_hline(yintercept = 0, linetype = 2)
```

Now, let's run some diagnostics on the model fit

```{r}
lltm4_full_diag_viz <- full_dlm_diagnostics_viz(dlm_model = lltm4_full_mod, lags = 10)
```

-   Visualization of residuals

```{r}
lltm4_full_diag_viz$residuals
```

-   Visualization of Ljung-Box test results (for independence)

```{r}
lltm4_full_diag_viz$box_ljung
```

The p-values are high enough that we do not have enough evidence to reject the null of independence.

-   Visualization of ACF (for independence)

```{r}
lltm4_full_diag_viz$acf
```

The ACF values are contained within the band, which indicates independence in the residuals.

-   Visualization of QQ-plot (for normality)

```{r}
lltm4_full_diag_viz$qqplot
```

The deviations from the line are not persistent so the qq plot points to normality

-   Diagnostic test results

```{r}
full_dlm_diagnostics(dlm_model = lltm4_full_mod, lags = 10) %>%
  flextable::flextable()
```

The local linear trend model with a stochastic level and slope appears to be a good fit for the log of Finnish road fatalities data!
