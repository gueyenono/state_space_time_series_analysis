# The local level model with explanatory variable

Dynamic linear models accommodate the inclusion of explanatory variables to explain the development of a particular series. In the case of *one* additional explanatory variable $x_t$, we have:

$$
\begin{align}
  y_t &= \mu_t + \alpha_{t} x_{t} + \varepsilon_{y,t}, & \mathcal{N}(0, \sigma_{y}^{2}) \\
  \mu_t &= \mu_{t-1} + \varepsilon_{\mu,t}, & \mathcal{N}(0, \sigma_{\mu}^{2}) \\
  \alpha_{t} &= \alpha_{t-1} + \varepsilon_{\alpha,t}, & \mathcal{N}(0, \sigma_{\alpha}^{2})
\end{align}
$$

where $\alpha_t$ is the coefficient. $\sigma_{\alpha}^{2}$ is usually set to zero so that the parameter is fixed (i.e. a stable relationship between $y_t$ and $x_t$). But it does not have to be.

## Deterministic level and explanatory variable

... write the math here!

The model can be reduced to the following regression equation:

$$
\begin{align}
  y_t &= \mu_{1} + \alpha_{1} x_{t} + \varepsilon_{t}, & \varepsilon_{t} \sim \mathcal{N}(\sigma_{y,t}^{2})
\end{align}
$$

Let's use **time** as an explanatory variable (i.e. $x_{t} = 1, 2, \dots$).

```{r}
library(magrittr) # for the pipe operator

# Import data
uk_ksi_raw <- readr::read_lines(file = here::here("data/UKdriversKSI.txt"), skip = 1)
uk_ksi <- tibble::tibble(
  date = seq(from = as.Date("1969-01-01"), by = "month", length.out = length(uk_ksi_raw)),
  value = as.numeric(uk_ksi_raw),
  log_value = log(value)
)

# Regressor: time
x <- as.numeric(seq_len(nrow(uk_ksi))) # Will not work if the data is integer and not numeric (no idea why!)

# Initial hyperparameter values
llmev1_initial_values <- var(uk_ksi$log_value)

# Function for building the model
func_llmev1_mod <- function(parm){
  mod1 <- dlm::dlmModPoly(
    order = 1,
    dV = exp(parm[1]),
    dW = 0
  )
  mod2 <- dlm::dlmModReg(
    X = x,
    dV = 0,
    addInt = FALSE
  )
  mod1 + mod2
}

# Maximum likelihood estimation of hyperparameters
llmev1_mle_est <- dlm::dlmMLE(
  y = uk_ksi$log_value,
  parm = log(llmev1_initial_values),
  build = func_llmev1_mod
)

# Do we have convergence?
llmev1_mle_est$convergence

# Build the model
llmev1_mod <- func_llmev1_mod(llmev1_mle_est$par)

# Kalman filter and smoother with the maximum likelihood estimate of the hyperparameter
llmev1_filtered <- dlm::dlmFilter(
  y = uk_ksi$log_value,
  mod = llmev1_mod
)

llmev1_smoothed <- dlm::dlmSmooth(
  y = llmev1_filtered,
  mod = llmev1_mod
)
```

Value of the log-likelihood at convergence

```{r}
# Here I'm using the master function in order to speed up the process
source(file = here::here("R/full_dlm_spec.R"))
source(file = here::here("R/full_visualizations.R"))
source(file = here::here("R/diagnostic_tests.R"))


llmev1a <- full_dlm_modeling(
  series = uk_ksi$log_value,
  state_components = c("level", "regressor"), 
  deterministic_components = "level",
  reg_data = x
)

llmev1a$loglik
```

Value of the log-likelihood to compare to the book's (book's value: $0.4140728$)

```{r}
llmev1a$loglik2
```

Maximum likelihood estimate of the observation variance

```{r}
llmev1a$obs_variance
```

Maximum likelihood estimation of the value of the deterministic level

```{r}
llmev1a$smoothed$s[2, 1]
```

Maximum likelihood estimation of the value of the regressor coefficient

```{r}
llmev1a$smoothed$s[2, 2]
```

Value of the AIC

```{r}
llmev1a$aic
```

Value of the AIC to compare to book's value ($-0.796896$)

```{r}
llmev1a$aic2
```


Now we use gas prices in the UK as an explanatory variable to the model. The idea is that higher/lower gas prices could affect the number of vehicles in traffic, which would also affect the number of road accidents.

```{r}
gas_price_data <- readr::read_lines(file = here::here("data/logUKpetrolprice.txt"), skip = 1) %>%
  as.numeric()

llmev1b <- full_dlm_modeling(
  series = uk_ksi$log_value,
  state_components = c("level", "regressor"), 
  deterministic_components = "level",
  reg_data = gas_price_data
)
```

Value of the log-likelihood to compare to the book's (book's value: $0.4457201$)

```{r}
llmev1b$loglik2
```

Maximum likelihood estimate of the observation variance

```{r}
llmev1b$obs_variance
```

Maximum likelihood estimation of the value of the deterministic level

```{r}
llmev1b$smoothed$s[2, 1]
```

Maximum likelihood estimation of the value of the regressor coefficient. The negative sign means that lower gas prices will lead to more people killed or seriously injured in traffic. Also, the fact that the dependent and explanatory variables are in log form, the coefficient can be interpreted as an *elasticity* coefficient.

```{r}
llmev1b$smoothed$s[2, 2]
```

Value of the AIC

```{r}
llmev1b$aic
```

Value of the AIC to compare to book's value ($-0.796896$)

```{r}
llmev1b$aic2
```


## Stochastic level and explanatory variable

```{r}
llmev2 <- full_dlm_modeling(
  series = uk_ksi$log_value,
  state_components = c("level", "regressor"),
  deterministic_components = NULL,
  reg_data = gas_price_data
)
```

Value of the log-likelihood at convergence

```{r}
llmev2$loglik
```

Value of the log-likelihood to compare with the book's (book's value: $0.6456361$)

```{r}
llmev2$loglik2
```

Variance of the observation disturbances

```{r}
llmev2$obs_variance
```

Initial value of the level

```{r}
llmev2$smoothed$s[2, 1]
```

Initial value of the regressing coefficient

```{r}
llmev2$smoothed$s[2, 2]
```

AIC

```{r}
llmev2$aic
```

AIC to compare to book's value (book value: $-1.24961$)

```{r}
llmev2$aic2
```

