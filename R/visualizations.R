visualize_residuals <- function(dlm_filtered, dlm_smoothed){
  
  viz_data <- tibble::tibble(
    t = seq_along(dlm_filtered$y),
    y = dlm_filtered$y,
    residuals = {
      if("numeric" %in% class(dlm_smoothed$s)) out <- dlm_smoothed$s[-1]
      if("matrix" %in% class(dlm_smoothed$s)) out <- dlm_smoothed$s[-1, 1]
      y - out
    }
  )
  
  ggplot2::ggplot(data = viz_data, mapping = ggplot2::aes(x = t, y = residuals)) +
    ggplot2::geom_line() +
    ggplot2::geom_hline(yintercept = 0, linetype = 2) +
    ggplot2::labs(x = NULL, y = NULL) +
    ggplot2::theme_bw()
}


visualize_level <- function(dlm_filtered, dlm_smoothed, series_name){
  
  viz_data <- tibble::tibble(
    t = seq_along(dlm_filtered$y),
    y = dlm_filtered$y,
    smooth_state = {
      if("numeric" %in% class(dlm_smoothed$s)) out <- dlm_smoothed$s[-1]
      if("matrix" %in% class(dlm_smoothed$s)) out <- dlm_smoothed$s[-1, 1]
      out
    }
  )
  
  ggplot2::ggplot(data = viz_data, ggplot2::aes(x = t)) +
    ggplot2::geom_line(mapping = ggplot2::aes(y = y, color = "series")) +
    ggplot2::geom_line(mapping = ggplot2::aes(y = smooth_state, color = "state"), linetype = 2) +
    ggplot2::labs(x = NULL, y = NULL, color = NULL) +
    ggplot2::scale_color_manual(
      values = c("series" = "black", "state" = "red"),
      labels = c("series" = series_name, "state" = "Smooth level")
    ) +
    ggplot2::theme_bw()
  
}


visualize_other_state_components <- function(dlm_filtered, dlm_smoothed, component_names){
  
  components_values_list <- tibble::as_tibble(dlm_smoothed$s)[-1 ,-1] %>% as.list()
  
  purrr::map2(components_values_list, component_names, function(d, comp_name){
    
    viz_data <- tibble::tibble(
      t = seq_along(dlm_filtered$y),
      comp = d
    )
    
    ggplot2::ggplot(data = viz_data[-1,], mapping = ggplot2::aes(x = t, y = comp)) +
      ggplot2::geom_line() +
      ggplot2::labs(x = "Time period", y = NULL, title = "State component:", subtitle = comp_name) +
      ggplot2::theme_bw()
    
  })
  
}




