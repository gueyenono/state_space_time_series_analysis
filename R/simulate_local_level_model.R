# Simulate local level model with no level disturbance (deterministic level)

simulate_llm1 <- function(initial_level, n = 100) {
  y <- initial_level + cumsum(rnorm(n = n, mean = 0, sd = 1))
  tibble::tibble(t = seq_along(y), y = y)
}


# Simulate a local level model with a stochastic level

simulate_llm2 <- function(n = 100, initial_level, obs_var = 100, level_var = 50){
  level <- initial_level + cumsum(rnorm(n = n, mean = 0, sd = sqrt(level_var)))
  y <- level + cumsum(rnorm(n = n, mean = 0, sd = sqrt(level_var)))
  tibble::tibble(t = seq_along(y), level = level, y = y)
}
